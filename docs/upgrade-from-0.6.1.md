Recently slack changed the way incoming webhooks are handled. Instead of taking a team name and token, they now provide a unique (obfuscated) webhook url.

To upgrade the slack-messenger gem, you'll need to find your webhook url. In slack:
- go to you're configured integrations (https://team-name.slack.com/services)
- select **Incoming Webhooks**
- select the webhook that uses the slack-messenger gem
- find the webhook url under the heading **Integration Settings**

You'll then change the way you initialize your messenger

From:
```ruby
messenger = Slack::Messenger.new 'team', 'token'
```

To:
```ruby
messenger = Slack::Messenger.new 'WEBHOOK_URL'
```

Defaults & attachemnts will continue to work like they have

```ruby
messenger = Slack::Messenger.new 'WEBHOOK_URL', icon_emoji: ":ghost:"
messenger.ping "I'm feeling spooky"
```
